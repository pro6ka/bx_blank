# bx_blank

bitrix blank project

## В проект входят
1. \Kint-php\Kint <https://packagist.org/packages/kint-php/kint>

## Установка
1. Создайте репозиторий на уровень выше DOCUMENT_ROOT. ``` git init ```
2. Добавлить удалённый репозиторий ``` git remote add blank git@bitbucket.org:pro6ka/bx_blank.git ```
3. Склонировать репозиторий ``` git pull blank master ```

## composer-bx.json
    В настоящее время не используется по причине не совместимосьи версий symfony/console в библиотеке битрикса и в notamedia/console-jedi.
    Возможно с развитием битрикса или console-jedi от одного изрешений можно будет избавиться окончательно.
