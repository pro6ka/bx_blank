<?php

require $_SERVER['DOCUMENT_ROOT'] . '/../vendor/autoload.php';

if (! function_exists('dd')) {
    function dd($data, $die = true) {
        global $USER;
        if (! $USER->IsAdmin()) {
            return;
        }
        echo '<pre>';
        echo '<hr>';
        debug_print_backtrace(DEBUG_BACKTRACE_IGNORE_ARGS, 3);
        var_dump($data);
        echo '<hr>';
        echo '</pre>';
        if (die) {
            die;
        }
    }
};
