<?
if(!defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED!==true)die();
IncludeTemplateLangFile(__FILE__);
?>
<footer id="colophon" class="site-footer" role="contentinfo">
			
			<!-- layout-medium -->
			<div class="layout-medium">
				
				<!-- footer-social -->
				<div class="footer-social">
					
					<ul class="social">
					<?
						$APPLICATION->IncludeComponent('bitrix:main.include','',array(
						"AREA_FILE_SHOW" => "file",
						"PATH" => SITE_DIR."include/invicto/different/social.php",
						));
					?>
					</ul>
					
				</div>
				<div class="site-info">
					
					<p>
						<?
							$APPLICATION->IncludeComponent('bitrix:main.include','',array(
							"AREA_FILE_SHOW" => "file",
							"PATH" => SITE_DIR."include/invicto/different/footer.php",
							));
						?>
					</p>
	
				</div>
			</div>
		</footer>
	</div>
	<?
		$APPLICATION->IncludeComponent('bitrix:main.include','',array(
		"AREA_FILE_SHOW" => "file",
		"PATH" => SITE_DIR."include/invicto/footer_form.php",
		));
	?>


<?$APPLICATION->ShowHeadScripts();?>
</body>
</html>