<?php

use Bitrix\Main\Loader;
use Bitrix\Main\Localization\Loc;

if (!defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED !== true) die();
Loc::loadMessages(__FILE__);

try {
    Loader::includeModule('.shantilab.bxeco');
    Loader::includeModule('iblock');
} catch (\Exception $e) {
    echo '<pre>';
    var_dump($e->getMessage());
}
?>
<!doctype html>
<html>
<head>
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <title><? $APPLICATION->ShowTitle() ?></title>
    <? $APPLICATION->ShowMeta('keyword'); ?>
    <? $APPLICATION->ShowMeta('description'); ?>
    <? $APPLICATION->ShowMeta('content'); ?>
    <? $APPLICATION->ShowMeta('autor'); ?>
    <? $APPLICATION->ShowHeadStrings(); ?>
    <!-- FAV and TOUCH ICONS -->
    <link rel="icon" type="image/x-icon"
          href="<? echo SITE_TEMPLATE_PATH ?>/images/favicon.ico"/>
    <link rel="apple-touch-icon" href="<? echo SITE_TEMPLATE_PATH ?>/images/ico/apple-touch-icon.png">
    <!-- FONTS -->
    <? $APPLICATION->SetAdditionalCSS(SITE_TEMPLATE_PATH . '/css/fonts/montserrat/montserrat.css') ?>
    <link href='http://fonts.googleapis.com/css?family=Roboto:400,400italic,700,700italic' rel='stylesheet'
          type='text/css'>
    <!-- STYLES -->
    <? $APPLICATION->SetAdditionalCSS(SITE_TEMPLATE_PATH . '/css/bootstrap.min.css') ?>
    <? $APPLICATION->SetAdditionalCSS(SITE_TEMPLATE_PATH . '/css/fonts/fontello/css/fontello.css') ?>
    <? $APPLICATION->SetAdditionalCSS(SITE_TEMPLATE_PATH . '/js/jquery.uniform/uniform.default.css') ?>
    <? $APPLICATION->SetAdditionalCSS(SITE_TEMPLATE_PATH . '/js/jquery.fluidbox/fluidbox.css') ?>
    <? $APPLICATION->SetAdditionalCSS(SITE_TEMPLATE_PATH . '/js/owl-carousel/owl.carousel.css') ?>
    <? $APPLICATION->SetAdditionalCSS(SITE_TEMPLATE_PATH . '/js/photo-swipe/photoswipe.css') ?>
    <? $APPLICATION->SetAdditionalCSS(SITE_TEMPLATE_PATH . '/js/photo-swipe/default-skin/default-skin.css') ?>
    <? $APPLICATION->SetAdditionalCSS(SITE_TEMPLATE_PATH . '/js/jquery.magnific-popup/magnific-popup.css') ?>
    <? $APPLICATION->SetAdditionalCSS(SITE_TEMPLATE_PATH . '/js/slippry/slippry.css') ?>
    <? $APPLICATION->SetAdditionalCSS(SITE_TEMPLATE_PATH . '/css/style.css') ?>
    <? $APPLICATION->SetAdditionalCSS(SITE_TEMPLATE_PATH . '/css/main.css') ?>
    <? $APPLICATION->SetAdditionalCSS(SITE_TEMPLATE_PATH . '/css/768.css') ?>
    <? $APPLICATION->SetAdditionalCSS(SITE_TEMPLATE_PATH . '/css/992.css') ?>

    <!-- scripts -->
    <? $APPLICATION->AddHeadScript(SITE_TEMPLATE_PATH . '/js/jquery-1.11.2.min.js') ?>
    <? $APPLICATION->AddHeadScript(SITE_TEMPLATE_PATH . '/js/jquery-migrate-1.2.1.min.js') ?>
    <? $APPLICATION->AddHeadScript(SITE_TEMPLATE_PATH . '/js/modernizr.min.js') ?>
    <? $APPLICATION->AddHeadScript(SITE_TEMPLATE_PATH . '/js/fastclick.js') ?>
    <? $APPLICATION->AddHeadScript(SITE_TEMPLATE_PATH . '/js/jquery.fitvids.js') ?>
    <? $APPLICATION->AddHeadScript(SITE_TEMPLATE_PATH . '/js/jquery.validate.min.js') ?>
    <? $APPLICATION->AddHeadScript(SITE_TEMPLATE_PATH . '/js/jquery.uniform/jquery.uniform.min.js') ?>
    <? $APPLICATION->AddHeadScript(SITE_TEMPLATE_PATH . '/js/imagesloaded.pkgd.min.js') ?>
    <? $APPLICATION->AddHeadScript(SITE_TEMPLATE_PATH . '/js/jquery.fluidbox/jquery.fluidbox.min.js') ?>
    <? $APPLICATION->AddHeadScript(SITE_TEMPLATE_PATH . '/js/owl-carousel/owl.carousel.min.js') ?>
    <? $APPLICATION->AddHeadScript(SITE_TEMPLATE_PATH . '/js/socialstream.jquery.js') ?>
    <? $APPLICATION->AddHeadScript(SITE_TEMPLATE_PATH . '/js/jquery.collagePlus/jquery.collagePlus.min.js') ?>
    <? $APPLICATION->AddHeadScript(SITE_TEMPLATE_PATH . '/js/photo-swipe/photoswipe.min.js') ?>
    <? $APPLICATION->AddHeadScript(SITE_TEMPLATE_PATH . '/js/photo-swipe/photoswipe-ui-default.min.js') ?>
    <? $APPLICATION->AddHeadScript(SITE_TEMPLATE_PATH . '/js/photo-swipe/photoswipe-run.js') ?>
    <? $APPLICATION->AddHeadScript(SITE_TEMPLATE_PATH . '/js/jquery.gridrotator.js') ?>
    <? $APPLICATION->AddHeadScript(SITE_TEMPLATE_PATH . '/js/slippry/slippry.min.js') ?>
    <? $APPLICATION->AddHeadScript(SITE_TEMPLATE_PATH . '/js/jquery.magnific-popup/jquery.magnific-popup.min.js') ?>
    <? $APPLICATION->AddHeadScript(SITE_TEMPLATE_PATH . '/js/masonry.pkgd.min.js') ?>
    <? $APPLICATION->AddHeadScript(SITE_TEMPLATE_PATH . '/js/main.js') ?>
    <? $APPLICATION->AddHeadScript(SITE_TEMPLATE_PATH . '/js/bootstrap.min.js') ?>


    <? $APPLICATION->ShowCSS(); ?>
    <?php
    $obElements = CIBlockElement::GetList(
        array('SORT' => 'ASC'),
        array(
            'IBLOCK_ID' => 3,
        ),
        false,
        false,
        false
    );
    $elements = [];
    while ($element = $obElements->Fetch()) {
        $elements[] = $element;
    }
    ?>

</head>

<body>
<div id="panel"><? $APPLICATION->ShowPanel(); ?></div>
<div id="page" class="hfeed site">
    <div class="layout-full">
        <header id="masthead" class="site-header" role="banner">
            <div class="row">
                <div class="hidden-xs col-sm-4">
                    <p class="head-adr">
                        <?
                        $APPLICATION->IncludeComponent('bitrix:main.include', '', array(
                            "AREA_FILE_SHOW" => "file",
                            "PATH" => SITE_DIR . "include/invicto/index/address.php",
                        ));
                        ?>
                    </p>
                    <?
                    $APPLICATION->IncludeComponent('bitrix:main.include', '', array(
                        "AREA_FILE_SHOW" => "file",
                        "PATH" => SITE_DIR . "include/invicto/index/scheme.php",
                    ));
                    ?>
                </div>
                <div class="col-sm-4">
                    <div class="site-logo">
                        <h1 class="site-title">
                            <a href="<?= SITE_DIR ?>" rel="home">
                                <?
                                $APPLICATION->IncludeComponent('bitrix:main.include', '', array(
                                    "AREA_FILE_SHOW" => "file",
                                    "PATH" => SITE_DIR . "include/invicto/different/pic_logo.php",
                                ));
                                ?>
                            </a>
                        </h1>
                    </div>
                </div>
                <div class="hidden-xs col-sm-4">
                    <p class="head-adr">
                        <?
                        $APPLICATION->IncludeComponent('bitrix:main.include', '', array(
                            "AREA_FILE_SHOW" => "file",
                            "PATH" => SITE_DIR . "include/invicto/index/telephone.php",
                        ));
                        ?>
                    </p>
                    <a href="#" data-toggle="modal" data-target="#myModal"
                       class="head-link go-modal"><?= GetMessage('CALL_ME') ?></a>
                </div>
            </div>
            <nav id="primary-navigation" class="site-navigation primary-navigation" role="navigation">

                <a class="menu-toggle"><span class="lines"></span></a>

                <!-- nav-menu -->
                <? $APPLICATION->IncludeComponent("bitrix:menu", "invicto", Array(
                    "ROOT_MENU_TYPE" => "top",    // ��� ���� ��� ������� ������
                    "MAX_LEVEL" => "1",    // ������� ����������� ����
                    "CHILD_MENU_TYPE" => "top",    // ��� ���� ��� ��������� �������
                    "USE_EXT" => "Y",    // ���������� ����� � ������� ���� .���_����.menu_ext.php
                    "DELAY" => "N",    // ����������� ���������� ������� ����
                    "ALLOW_MULTI_SELECT" => "Y",    // ��������� ��������� �������� ������� ������������
                    "MENU_CACHE_TYPE" => "N",    // ��� �����������
                    "MENU_CACHE_TIME" => "3600",    // ����� ����������� (���.)
                    "MENU_CACHE_USE_GROUPS" => "Y",    // ��������� ����� �������
                    "MENU_CACHE_GET_VARS" => "",    // �������� ���������� �������
                ),
                    false
                ); ?>
            </nav>
        </header>
    </div>
