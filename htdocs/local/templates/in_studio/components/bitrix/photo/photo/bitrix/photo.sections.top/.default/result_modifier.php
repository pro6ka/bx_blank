<?if(!defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED!==true)die();

foreach($arResult['SECTIONS'] as $key => $arSection)
{
	if(!empty($arSection['PICTURE']))
		$arResult['SECTIONS'][$key]['PICTURE'] = CFile::GetFileArray($arSection['PICTURE']);
}