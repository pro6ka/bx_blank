<?if(!defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED!==true)die();

$this->setFrameMode(true);
?>
<div id="main" class="site-main">
    <div id="primary" class="content-area">
        <div id="content" class="site-content" role="main">
            <div class="layout-full">
                <div class="media-grid-wrap">
					<div class="gallery-grid">
						<?foreach($arResult["SECTIONS"] as $arSection):?>
						<div class="masonry-item">
						<figure> 
							<img src="<?=$arSection['PICTURE']['SRC']?>" alt="gallery-post">
							<figcaption>
								<h2><?php echo $arSection["NAME"] ?></h2>
								<p><?php echo $arSection["DESCRIPTION"] ?></p>
								<a href="<?php echo $arSection["SECTION_PAGE_URL"]  ?>"></a>
							</figcaption>
						</figure>
						</div>
						<?endforeach;?>
					</div>
                </div>
            </div>
        </div>
    </div>
</div>