<?if(!defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED!==true)die();

$this->setFrameMode(true);
?>
<div id="main" class="site-main">
    <div id="primary" class="content-area">
        <div id="content" class="site-content" role="main">
            <div class="layout-full">
                <div class="gallery-single">
                    <header class="entry-header">
                        <h1 class="entry-title"><?php echo $arResult["NAME"] ?></h1>
                    </header>
                    <div class="media-grid-wrap">
                        
        				<? if (!empty($arResult["ITEMS"]) && count($arResult['ITEMS'])>0): ?>
        				<div class="mfp-gallery pw-collage pw-collage-loading" data-row-height="360" data-effect="effect-4">
        				    <?foreach($arResult["ITEMS"] as $arItem):?>
                                <a href="<?=$arItem['DETAIL_PICTURE']['SRC']?>" title="<?=$arItem['DETAIL_PICTURE']['TITLE']?>">
                                    <img src="<?=$arItem['PREVIEW_PICTURE']['SRC']?>" alt="<?=$arItem['DETAIL_PICTURE']['ALT']?>">
                                </a>
                            <?endforeach;?>
                        </div>
                        <? else: ?>
                        <p><?=GetMessage('SECTION_IS_UNDER_CONSTRUCTION')?></p>
                        <? endif; ?>

                    </div>
<?if($arParams["DISPLAY_BOTTOM_PAGER"]):?>
	<?=$arResult["NAV_STRING"]?>
<?endif;?>
				</div>
            </div>
        </div>
    </div>
</div>