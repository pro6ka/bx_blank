<?if(!defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED!==true)die();
$this->setFrameMode(true);
?>

	<? foreach ($arResult['ITEMS'] as $arItem ): ?>
	<div class="col-sm-4" id="<?=$this->GetEditAreaId($arItem['ID']);?>">
		<div class="pricing-table">
			<h3><?php echo $arItem["NAME"] ?></h3>
			<h4><?php echo $arItem["DISPLAY_PROPERTIES"]["t_price"]["DISPLAY_VALUE"] ?></h4>
			<?php echo $arItem["DISPLAY_PROPERTIES"]["t_desk"]["DISPLAY_VALUE"] ?>
			<a class="button" href="/contacts/"><?php echo GetMessage('CONTACT') ?></a>
		</div>
	</div>
	<? endforeach ?>