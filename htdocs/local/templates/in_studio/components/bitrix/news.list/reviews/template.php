<?if(!defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED!==true)die();
$this->setFrameMode(true);
?>

<? foreach ($arResult['ITEMS'] as $key => $arItem ): ?>
	<?php if ($key%2==0)  echo '<div class="col-sm-4">'; ?>
		<div class="testo">
			<img src="<?php echo $arItem["PREVIEW_PICTURE"]["SRC"] ?>">
			<h4><?php echo $arItem["NAME"] ?>
			    <span><?php echo $arItem["DISPLAY_PROPERTIES"]["t_pro"]["DISPLAY_VALUE"] ?></span>
			</h4>
			<p><?php echo $arItem["DISPLAY_PROPERTIES"]["t_com"]["DISPLAY_VALUE"] ?></p>
		</div>
	<?php if ($key%2==1)  echo '</div>'; ?>
<? endforeach ?>

