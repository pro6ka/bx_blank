<?
$MESS ['TITLE'] = "Обратный звонок";
$MESS ['MFT_NAME'] = "Ваше имя";
$MESS ['MFT_NAME_DESC'] = "Имя";
$MESS ['MFT_MESSAGE'] = "Номер телефона";
$MESS ['MFT_MESSAGE_DESC'] = "Телефон";
$MESS ['MFT_CAPTCHA'] = "Защита от автоматических сообщений";
$MESS ['MFT_CAPTCHA_CODE'] = "Введите слово на картинке";
$MESS ['MFT_SUBMIT'] = "Перезвоните мне";
?>