<?
if(!defined("B_PROLOG_INCLUDED")||B_PROLOG_INCLUDED!==true)die();
$this->setFrameMode(true);
?>
<div class="modal fade" id="myModal" tabindex="-1" role="dialog" aria-labelledby="myModalLabel">
    <div class="modal-dialog" role="document">
    <div class="modal-content">
      <div class="modal-header">
        <button type="button" class="close" data-dismiss="modal" aria-hidden="true">X</button>
        <h4 class="modal-title" id="myModalLabel"><?=GetMessage('TITLE')?></h4>
      </div>
	<form role="form" action="<?=POST_FORM_ACTION_URI?>" method="POST" id="feedback_form">
      <div class="modal-body">
		<div class="post-status">
<?if(!empty($arResult["ERROR_MESSAGE"]))
{
	$APPLICATION->RestartBuffer();
	foreach($arResult["ERROR_MESSAGE"] as $v)
		ShowError($v);
	die();
}
if(strlen($arResult["OK_MESSAGE"]) > 0)
{
	$APPLICATION->RestartBuffer();
	?><div class="mf-ok-text"><?=$arResult["OK_MESSAGE"]?></div><?
	die();
}
?>
		</div>
			<?=bitrix_sessid_post()?>
			  <div class="form-group">
				<label for="name"><?=GetMessage('MFT_NAME')?></label>
				<input type="text" name="user_name" class="form-control" value="<?=$arResult["AUTHOR_NAME"]?>" id="name" placeholder="<?=GetMessage('MFT_NAME_DESC')?>">
			  </div>
			  <div class="form-group">
				<label for="message"><?=GetMessage('MFT_MESSAGE')?></label>
				<input type="tel" name="MESSAGE" class="form-control" value="<?=$arResult["MESSAGE"]?>" id="message" placeholder="<?=GetMessage('MFT_MESSAGE_DESC')?>">
			  </div>
	<?if($arParams["USE_CAPTCHA"] == "Y"):?>
			  <div class="form-group">
				<div class="mf-text"><?=GetMessage("MFT_CAPTCHA")?></div>
				<input type="hidden" name="captcha_sid" value="<?=$arResult["capCode"]?>">
				<img src="/bitrix/tools/captcha.php?captcha_sid=<?=$arResult["capCode"]?>" width="180" height="40" alt="CAPTCHA">
				<div class="mf-text"><?=GetMessage("MFT_CAPTCHA_CODE")?><span class="mf-req">*</span></div>
				<input type="text" name="captcha_word" size="30" maxlength="50" value="">
			  </div>
	<?endif;?>
      </div>
      <div class="modal-footer">
		<input type="hidden" name="PARAMS_HASH" value="<?=$arResult["PARAMS_HASH"]?>">
        <button type="submit" name="submit" value="1" class="btn btn-primary"><?=GetMessage("MFT_SUBMIT")?></button>
      </div>
	</form>
  </div>
  </div>
</div>