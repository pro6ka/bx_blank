<?if(!defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED!==true)die();
/** @var array $arParams */
/** @var array $arResult */
/** @global CMain $APPLICATION */
/** @global CUser $USER */
/** @global CDatabase $DB */
/** @var CBitrixComponentTemplate $this */
/** @var string $templateName */
/** @var string $templateFile */
/** @var string $templateFolder */
/** @var string $componentPath */
/** @var CBitrixComponent $component */
$this->setFrameMode(true);

$strNavQueryString = ($arResult["NavQueryString"] != "" ? $arResult["NavQueryString"]."&amp;" : "");
$strNavQueryStringFull = ($arResult["NavQueryString"] != "" ? "?".$arResult["NavQueryString"] : "");
?>

<nav class="nav-single row">

<?if($arResult["bDescPageNumbering"] === true):?>

	<?if ($arResult["NavPageNomer"] < $arResult["NavPageCount"]):?>
		<?if($arResult["bSavePage"]):?>
	<div class="nav-previous col-xs-6">
		<a href="<?=$arResult["sUrlPath"]?>?<?=$strNavQueryString?>PAGEN_<?=$arResult["NavNum"]?>=<?=($arResult["NavPageNomer"]+1)?>"><h4><?=GetMessage('PREVIOUS')?></h4></a>
	</div>
		<?else:?>
			<?if ($arResult["NavPageCount"] == ($arResult["NavPageNomer"]+1) ):?>
	<div class="nav-previous col-xs-6">
		<a href="<?=$arResult["sUrlPath"]?><?=$strNavQueryStringFull?>"><h4><?=GetMessage('PREVIOUS')?></h4></a>
	</div>
			<?else:?>
	<div class="nav-previous col-xs-6">
		<a href="<?=$arResult["sUrlPath"]?>?<?=$strNavQueryString?>PAGEN_<?=$arResult["NavNum"]?>=<?=($arResult["NavPageNomer"]+1)?>"><h4><?=GetMessage('PREVIOUS')?></h4></a>
	</div>
			<?endif?>
		<?endif?>
	<?else:?>
	<div class="nav-previous col-xs-6">
	</div>
	<?endif?>
	<?if ($arResult["NavPageNomer"] > 1):?>
	<div class="nav-next col-xs-6">
		<a href="<?=$arResult["sUrlPath"]?>?<?=$strNavQueryString?>PAGEN_<?=$arResult["NavNum"]?>=1"><h4><?=GetMessage("NEXT")?></h4></a>
	</div>  
	<?endif?>

<?else:?>

	<?if ($arResult["NavPageNomer"] > 1):?>

		<?if($arResult["bSavePage"]):?>
	<div class="nav-previous col-xs-6">
		<a href="<?=$arResult["sUrlPath"]?>?<?=$strNavQueryString?>PAGEN_<?=$arResult["NavNum"]?>=<?=($arResult["NavPageNomer"]-1)?>"><h4><?=GetMessage('PREVIOUS')?></h4></a>
	</div>
		<?else:?>
			<?if ($arResult["NavPageNomer"] > 2):?>
	<div class="nav-previous col-xs-6">
		<a href="<?=$arResult["sUrlPath"]?>?<?=$strNavQueryString?>PAGEN_<?=$arResult["NavNum"]?>=<?=($arResult["NavPageNomer"]-1)?>"><h4><?=GetMessage('PREVIOUS')?></h4></a>
	</div>
			<?else:?>
	<div class="nav-previous col-xs-6">
		<a href="<?=$arResult["sUrlPath"]?><?=$strNavQueryStringFull?>"><h4><?=GetMessage('PREVIOUS')?></h4></a>
	</div>
			<?endif?>
		<?endif?>
	<?else:?>
	<div class="nav-previous col-xs-6">
	</div>
	<?endif?>

	<?if($arResult["NavPageNomer"] < $arResult["NavPageCount"]):?>
	<div class="nav-next col-xs-6">
		<a href="<?=$arResult["sUrlPath"]?>?<?=$strNavQueryString?>PAGEN_<?=$arResult["NavNum"]?>=<?=($arResult["NavPageNomer"]+1)?>"><h4><?=GetMessage("NEXT")?></h4></a>
	</div> 
	<?endif?>

<?endif?>

</nav>