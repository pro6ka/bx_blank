<?php
if (!defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED !== true) die();
$this->setFrameMode(true);
?>
<?php if (!empty($arResult)) { ?>
    <div class="nav-menu">
    <ul>
    <?php foreach ($arResult as $arItem) { ?>
        <?php if ($arItem["SELECTED"]) { ?>
            <li><a href="<?= $arItem["LINK"] ?>" class="selected"><?= $arItem["TEXT"] ?></a></li>
        <?php } else { ?>
            <li><a href="<?= $arItem["LINK"] ?>"><?= $arItem["TEXT"] ?></a></li>
        <?php } ?>
    <?php } ?>
        </ul>
        </div>
<?php } ?>
