<?php
require($_SERVER["DOCUMENT_ROOT"]."/bitrix/header.php");
$APPLICATION->SetTitle("Обо мне");
?>


<!-- site-main -->
<div id="main" class="site-main">
    
    <!-- primary -->
	<div id="primary" class="content-area">
    	
        <!-- site-content -->
        <div id="content" class="site-content" role="main">

        	<div class="layout-medium">

                <article class="hentry">
                
                	
                    <!-- .entry-header -->   
                    <header class="entry-header">
                        <h1 class="entry-title">
                        	<?
                        	    $APPLICATION->IncludeComponent('bitrix:main.include','',array(
                        	    "AREA_FILE_SHOW" => "file",
                        	    "PATH" => SITE_DIR."include/invicto/about/h1.php",
                        	    ));
                        	?> 
                        </h1>
                    </header>
                    <!-- .entry-header -->   
                    
                    
                    <!-- .entry-content -->
                    <div class="entry-content">
                        
                        <div class="full-width-image">
                        	<?
                                $APPLICATION->IncludeComponent('bitrix:main.include','',array(
                                "AREA_FILE_SHOW" => "file",
                                "PATH" => SITE_DIR."include/invicto/about/pic_about.php",
                                ));
                            ?>
                        </div>
                        
                        <h2 class="section-title center">
                        <?
                            $APPLICATION->IncludeComponent('bitrix:main.include','',array(
                            "AREA_FILE_SHOW" => "file",
                            "PATH" => SITE_DIR."include/invicto/about/whois.php",
                            ));
                        ?>
                        </h2>
                        
                        <p>
                        <?
                            $APPLICATION->IncludeComponent('bitrix:main.include','',array(
                            "AREA_FILE_SHOW" => "file",
                            "PATH" => SITE_DIR."include/invicto/about/whois_answer.php",
                            ));
                        ?>
                        </p>
                        
                        
                        
                        <h2 class="section-title center">
                        	<?
                        	    $APPLICATION->IncludeComponent('bitrix:main.include','',array(
                        	    "AREA_FILE_SHOW" => "file",
                        	    "PATH" => SITE_DIR."include/invicto/about/social.php",
                        	    ));
                        	?>
                        </h2>
                        <div class="footer-social">
                          <ul class="social">
                          <?
                              $APPLICATION->IncludeComponent('bitrix:main.include','',array(
                              "AREA_FILE_SHOW" => "file",
                              "PATH" => SITE_DIR."include/invicto/different/social.php",
                              ));
                          ?>
                          </ul>
                        </div>
                        
                        <h2 class="section-title center">
							<?
							    $APPLICATION->IncludeComponent('bitrix:main.include','',array(
							    "AREA_FILE_SHOW" => "file",
							    "PATH" => SITE_DIR."include/invicto/about/fun_facts.php",
							    ));
							?>
                        </h2>
                        
                        <!-- row -->
                        <div class="row">
                            
                            <?
                                $APPLICATION->IncludeComponent('bitrix:main.include','',array(
                                "AREA_FILE_SHOW" => "file",
                                "PATH" => SITE_DIR."include/invicto/about/facts.php",
                                ));
                            ?>

                        </div>
                        <!-- row -->
                        	
                        
                        
                    </div>
                    <!-- .entry-content -->
                    
                    
                </article>

            </div>

        </div>
        <!-- site-content -->
        
    </div>
    <!-- primary --> 

</div>
<!-- site-main -->

<?require($_SERVER["DOCUMENT_ROOT"]."/bitrix/footer.php");?>
