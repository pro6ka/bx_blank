<?
require($_SERVER["DOCUMENT_ROOT"]."/bitrix/header.php");
$APPLICATION->SetTitle("Контакты");
?>

        <div id="main" class="site-main">
            <div id="primary" class="content-area">
                <div id="content" class="site-content" role="main">
                        
                    <div class="layout-fixed">
                        <article class="hentry page">

                            <header class="entry-header">
                                <h1 class="entry-title">
                                    <?
                                        $APPLICATION->IncludeComponent('bitrix:main.include','',array(
                                        "AREA_FILE_SHOW" => "file",
                                        "PATH" => SITE_DIR."include/invicto/contacts/contactme.php",
                                        ));
                                    ?>
                                </h1>
                            </header>
                            <div class="entry-content">
                                
                                <h3 class="section-title">
                                    <?
                                        $APPLICATION->IncludeComponent('bitrix:main.include','',array(
                                        "AREA_FILE_SHOW" => "file",
                                        "PATH" => SITE_DIR."include/invicto/contacts/iam.php",
                                        ));
                                    ?>
                                </h3>
                                
                                <ul class="social">
                                    <?
                                        $APPLICATION->IncludeComponent('bitrix:main.include','',array(
                                        "AREA_FILE_SHOW" => "file",
                                        "PATH" => SITE_DIR."include/invicto/different/social.php",
                                        ));
                                    ?>
                                </ul>

                                <h2 class="section-title">
                                    <?
                                        $APPLICATION->IncludeComponent('bitrix:main.include','',array(
                                        "AREA_FILE_SHOW" => "file",
                                        "PATH" => SITE_DIR."include/invicto/contacts/reachme.php",
                                        ));
                                    ?>
                                </h2>
                                
                                <!-- row -->
                                <div class="row">
                                   <?
                                       $APPLICATION->IncludeComponent('bitrix:main.include','',array(
                                       "AREA_FILE_SHOW" => "file",
                                       "PATH" => SITE_DIR."include/invicto/contacts/place.php",
                                       ));
                                   ?>
                                </div>

                                <h3 class="section-title">
                                    <?
                                        $APPLICATION->IncludeComponent('bitrix:main.include','',array(
                                        "AREA_FILE_SHOW" => "file",
                                        "PATH" => SITE_DIR."include/invicto/contacts/mailme.php",
                                        ));
                                    ?>
                                </h3>
                                  
                                <!-- .contact-form -->
                                <div class="contact-form">
                                
                                    <?$APPLICATION->IncludeComponent(
	"bitrix:main.feedback", 
	"invicto", 
	array(
		"USE_CAPTCHA" => "N",
		"OK_TEXT" => "Спасибо, ваше сообщение принято.",
		"EMAIL_TO" => COption::GetOptionString('main','email_from'),
		"REQUIRED_FIELDS" => array(
			0 => "NAME",
			1 => "EMAIL",
			2 => "MESSAGE",
		),
		"EVENT_MESSAGE_ID" => array(
			0 => "7",
		),
		"COMPONENT_TEMPLATE" => "invicto"
	),
	false
);?>
                                    
                                </div>
                            </div>
                        </article>
                    </div>
                </div>
            </div>
        </div>

        <?$APPLICATION->IncludeComponent(
	"bitrix:map.yandex.view", 
	".default", 
	array(
		"INIT_MAP_TYPE" => "MAP",
		"MAP_DATA" => "a:4:{s:10:\"yandex_lat\";d:54.754436347404955;s:10:\"yandex_lon\";d:55.98995235581969;s:12:\"yandex_scale\";i:17;s:10:\"PLACEMARKS\";a:1:{i:0;a:3:{s:3:\"LON\";d:55.98995235581969;s:3:\"LAT\";d:54.754023691930165;s:4:\"TEXT\";s:1:\"Я\";}}}",
		"MAP_WIDTH" => "100%",
		"MAP_HEIGHT" => "500",
		"CONTROLS" => array(
			0 => "SCALELINE",
		),
		"OPTIONS" => array(
			0 => "ENABLE_DRAGGING",
		),
		"MAP_ID" => "yam_1",
		"COMPONENT_TEMPLATE" => ".default"
	),
	false
);?>

<?require($_SERVER["DOCUMENT_ROOT"]."/bitrix/footer.php");?>