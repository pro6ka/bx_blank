<div class="col-xs-6 col-sm-3">
    <div class="fun-fact">
        <i class="pw-icon-location-outline"></i>
        <h4>Нахожусь в Уфе, Россия</h4>
    </div>
</div>

<div class="col-xs-6 col-sm-3">
    <div class="fun-fact">
        <i class="pw-icon-phone-outline"></i>
        <h4>Тел: <br> +7 777 777 77 77</h4>
    </div>
</div>

<div class="col-xs-6 col-sm-3">
    <div class="fun-fact">
        <i class="pw-icon-mail-1"></i>
        <h4>johndoe@gmail.com</h4>
    </div>
</div>

<div class="col-xs-6 col-sm-3">
    <div class="fun-fact">
        <i class="pw-icon-thumbs-up"></i>
        <h4>Готов к сотрудничеству</h4>
    </div>
</div>